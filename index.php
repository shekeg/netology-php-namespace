<?php
  require_once('autoloader.php');

  echo '<br> ---Торвары--- <br>';

  $opel = new Shop\Product\Car('Opel', 1000, 190);

  $samsung = new Shop\Product\TV('Samsung', 90, 1);

  $ballPenBlack = new Shop\Product\BallPen('Black Pen', 5, 'black');

  $duck1 = new Shop\Product\Duck('Duck #1', 45, 'https://www.google.com/logos/fnbx/animal_sounds/duck.mp3');

  echo '<br> ---Корзина--- <br>';

  $basket = new Shop\Basket();

  $basket->addProduct($opel);
  $basket->addProduct($samsung);
  $basket->addProduct($ballPenBlack);
  $basket->deleteProduct($ballPenBlack);
  
  echo '<br> ---Заказ--- <br>';

  $order = new Shop\Order();
  $order->showOrder($basket);