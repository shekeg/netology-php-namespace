<?php
  spl_autoload_register(function($classNameWithNamespace) {
    $pathToFile = __DIR__ . DIRECTORY_SEPARATOR 
        . str_replace('\\', DIRECTORY_SEPARATOR, $classNameWithNamespace) . '.php';
    if (file_exists($pathToFile)) {
      include "$pathToFile";
    }
  });