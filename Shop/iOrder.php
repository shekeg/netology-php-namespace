<?php
  namespace Shop;

  interface iOrder {
    public function showOrder(iBasket $basket);
  }