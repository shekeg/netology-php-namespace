<?php
  namespace Shop\Product;

  interface iProduct {
    public function setPrice($price);
    public function setTitle($title);
  }