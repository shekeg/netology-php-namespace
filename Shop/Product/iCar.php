<?php
  namespace Shop\Product;

  interface iCar extends iProduct {
    public function go();
  }