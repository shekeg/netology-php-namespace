<?php
  namespace Shop\Product;

  final class TV extends Product implements iTV {
    private $channel;

    function __construct($title, $price, $channel) {
      $this->channel = $channel; 
      parent::__construct($title, $price);
    }

    public function changeChannel($channel) {
      $this->channel = $channel;
      echo "Выбранный канал {$channel}";
    }
  }