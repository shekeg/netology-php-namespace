<?php
  namespace Shop\Product;

  final class BallPen extends Product implements iBallPen{
    private $color;

    function __construct($title, $price, $color) {
      if ($color === 'blue' or $color === 'red' or $color === 'black')  {
        $this->color = $color;
      }
      parent::__construct($title, $price);
    }

    function write($text) {
      if (!empty($this->color)) {
        $color = $this->color;
      } else {
        die('Поддерживаемые цвета: blue, red, black');
      }
      $fontSize = 30;
      $width = 750;
      $height = 40;

      $image = imagecreate($width, $height);
      imagecolorallocate($image, 255, 255, 255);
      switch ($color) {
        case 'blue':
          $textColor = imagecolorallocate($image, 0, 0, 255);
          break;
        case 'red':
          $textColor = imagecolorallocate($image, 255, 0, 0);
          break;
        case 'black':
          $textColor = imagecolorallocate($image, 0, 0, 0);
          break;
      }
      imagettftext($image, $fontSize, 0, 15, 35, $textColor, __DIR__ . '/fonts/OpenSans.ttf', $text);

      header('Content-type: image/png');
      imagepng($image);
    }
  }