<?php
  namespace Shop\Product; 

  interface iBallPen extends iProduct {
    public function write($text);
  }