<?php
  namespace Shop\Product;

  class Car extends Product implements iCar{
    private $speed;

    function __construct($title, $price, $speed) {
      $this->speed = $speed;
      parent::__construct($title, $price);
    }

    public function go() {
      $speed = $this->speed;
      echo "Машине едет со скоростью {$speed}";
    }
  }