<?php
  namespace Shop\Product;
  
  interface iDuck extends iProduct {
    public function getSound();
  }