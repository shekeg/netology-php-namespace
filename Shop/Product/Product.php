<?php
  namespace Shop\Product;

  abstract class Product implements iProduct{
    private static $count = 0;
    private $article;
    private $title;
    private $price = 0;

    function __construct($title, $price) {
      $this->article = ++self::$count;
      $this->title = $title;
      $this->price = $price;
      echo '<pre>';
        var_dump($this);
      echo '</pre>';
    }

    public function getArticle() {
      return $this->article;
    }

    public function setPrice($price) {
      $this->price = $price;
      return $this;
    }

    public function getPrice() {
      return $this->price;
    }
    
    public function setTitle($title) {
      $this->title = $title;
      return $this;
    }

    public function getTitle() {
      return $this->title;
    }
  }