<?php
  namespace Shop\Product;

  interface iTV extends iProduct {
    public function changeChannel($channel);
  }