<?php
  namespace Shop\Product;

  final class Duck extends Product implements iDuck { 
    private $soundSource;

    function __construct($title, $price, $soundSource) {
      $this->soundSource = $soundSource;
      parent::__construct($title, $price);
    }

    public function getSound() {
      echo "<audio autoplay> <source src=\"{$this->soundSource}\" type=\"audio/mpeg\"> </audio>"; 
    }
  }