<?php
  namespace Shop;

  class Basket implements iBasket{
    private $productsInBasket = [];

    public function addProduct(Product\iProduct $product) {
      $article = $product->getArticle();
      $price = $product->getPrice();
      try {
        if (!$price or gettype($price) != 'integer') throw new \Exception('Товар без цены не может быть добавлен в карзину');
        $this->productsInBasket[$article] = $product;
        echo "В корзину добавлен {$product->getTitle()} <br>";
        return $this;
      } catch(\Exception $err) {
        die($err);
      }
    }

    public function deleteProduct(Product\iProduct $product) {
      $article = $product->getArticle();
      try {
        if (!array_key_exists($article, $this->productsInBasket)) throw new \Exception('Вы пытаетесь удалить товар, которого нет в карзине');
        unset($this->productsInBasket[$article]);
        echo "Из корзины удален {$product->getTitle()} <br>";
      } catch(\Exception $err) {
        echo($err);
      }
    }

    public function getTotalSum() {
      $totalSum = 0;
      foreach($this->productsInBasket as $key => $productInBasket) {
        $totalSum += $productInBasket->getPrice();
      }
      return $totalSum;
    }
  }