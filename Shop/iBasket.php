<?php
  namespace Shop; 

  interface iBasket {
    public function addProduct(Product\iProduct $product);
    public function deleteProduct(Product\iProduct $product);
    public function getTotalSum();
  }